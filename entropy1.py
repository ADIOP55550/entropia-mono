from typing import List
import functools

# ─── ORDERS ─────────────────────────────────────────────────────────────────────


def XuYager(a: 'Range', b: 'Range'):
    result = (a.low + a.high < b.low + b.high) or ((a.low + a.high ==
                                                    b.low + b.high) and (a.high - a.low <= b.high - b.low))
    # x = f'{a=}, {b=}, {result=}'
    # x
    return result


def Lex1(a: 'Range', b: 'Range'):
    result = a.low < b.low or (a.low == b.low and a.high <= b.high)
    # x = f'{a=}, {b=}, {result=}'
    # x
    return result


def Partial(a: 'Range', b: 'Range'):
    result = a.low <= b.low and a.high <= b.high
    # x = f'{a=}, {b=}, {result=}'
    # x
    return result


# USED ORDER
order = Lex1

# ─── NEGATIONS ──────────────────────────────────────────────────────────────────


def negate1(r: 'Range'):
    return Range(1-r.high, 1-r.low)


def negate2(r: 'Range'):
    return Range(1-(r.high*r.high), 1-(r.low*r.low))


def negate3(r: 'Range'):
    return Range(0, 0) if r.low == 1 and r.high == 1 else Range(1-r.low, 1)


# USED NEGATION
negate = negate1

# ─── AGGREGATIONS ───────────────────────────────────────────────────────────────


def mean(a, b):
    return (a+b)/2.0


def aggregate_max(r1: 'Range', r2: 'Range'):
    return Range(max(r1.low, r2.low), max(r1.high, r2.high))


def aggregate_min(r1: 'Range', r2: 'Range'):
    return Range(min(r1.low, r2.low), min(r1.high, r2.high))


def aggregate_mean(r1: 'Range', r2: 'Range'):
    return Range(mean(r1.low, r2.low), mean(r1.high, r2.high))


# USED AGGREGATION
aggregate = aggregate_mean

# ─── RANGE CLASS ────────────────────────────────────────────────────────────────


class Range:
    low: float
    high: float

    def __init__(self, low, high):
        self.low = low
        self.high = high

    def __repr__(self):
        return f'Range({self.low:.2f}, {self.high:.2f})'

    def __str__(self):
        return f'[{self.low:.2f}, {self.high:.2f}]'

    @staticmethod
    def fromList(l):
        return Range(l[0], l[1])

    @property
    def range(self):
        return self.high - self.low

    @staticmethod
    def compare(a, b):
        return 1 if a > b else -1 if b > a else 0

    def __lt__(self, other):
        return order(self, other) and not order(other, self)

    def __gt__(self, other):
        return order(other, self) and not order(self, other)

    def __le__(self, other):
        return order(self, other)

    def __ge__(self, other):
        return order(other, self)

    def __eq__(self, other):
        return order(self, other) and order(other, self)

    def __ne__(self, other):
        return not self == other

    def __neg__(self):
        return ~self

    def __invert__(self):
        return negate(self)

    def _or(self, other):
        return Range(max(self.low, other.low), max(self.high, other.high))

    def _and(self, other):
        return Range(min(self.low, other.low), min(self.high, other.high))


# TEST
assert aggregate_max(Range(0.0, 1.0), Range(0.5, 1.0)).low == 0.5
assert aggregate_max(Range(0.0, 1.0), Range(0.5, 1.0)).high == 1

# ─── PRECEDENCE ─────────────────────────────────────────────────────────────────


def prec(a: Range, b: Range):
    if a < b:
        return Range(1, 1)
    if a == b:
        return Range(1-a.range, 1)
    return aggregate(negate(a), b)

# ─── ENTROPY ────────────────────────────────────────────────────────────────────


def entropy(l: List[Range]):
    precs = []
    for e in l:
        x = f'''{e=}
        {(-e)=}
        {prec(e, -e)=}
        {prec(-e, e)=}
        {prec(e, -e)._and(prec(-e, e))=}'''
        x
        precs.append(prec(e, -e)._and(prec(-e, e)))

    result = functools.reduce(aggregate, precs)
    return result


def main():
    print("USED ORDER: " + order.__name__)
    print("USED NEGATION: " + negate.__name__)
    print("USED AGGREGATION: " + aggregate.__name__)

    A = [Range(0.0, 1.0),
         Range(0.5, 1.0),
         Range(0.6, 0.8),
         Range(0.4, 0.7),
         Range(0.0, 1.0),
         Range(0.0, 1.0)]

    B = list(map(Range.fromList, [[0.4, 0.7], [0.0, 1.0], [
        0.6, 0.9], [0.4, 0.8], [1.0, 1.0], [1.0, 1.0]]))

    C = list(map(Range.fromList, [[1.0, 1.0], [0.0, 1.0], [
        0.9, 0.9], [0.0, 1.0], [1.0, 1.0], [1.0, 1.0]]))

    D = list(map(Range.fromList, [[0.4, 0.7], [0.2, 1.0], [
        0.2, 1.0], [0.4, 0.8], [0.5, 1.0], [0.5, 0.8]]))

    E = list(map(Range.fromList, [[0.5, 0.6], [0.5, 1.0], [
        0.6, 0.8], [0.5, 0.9], [0.8, 1.0], [0.8, 1.0]]))

    entropies = {
        'A': entropy(A),
        'B': entropy(B),
        'C': entropy(C),
        'D': entropy(D),
        'E': entropy(E),
    }

    print()
    print('entropies:')
    print(entropies)

    print()
    print('sorted:')
    print(dict(sorted(entropies.items(), key=lambda item: item[1])))


if __name__ == '__main__':
    main()
